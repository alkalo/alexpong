﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball2 : MonoBehaviour {

	public Vector2 speed;

	private Vector2 iniSpeed;
	private Vector3 iniPos;
	public GameObject block;

	// Use this for initialization
	void Start () 
	{
		iniSpeed = speed;
		iniPos = transform.position;
	}

	// Update is called once per frame
	void Update () 
	{
		transform.Translate (speed.x*Time.deltaTime, speed.y*Time.deltaTime, 0);
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "Border") 
		{
			speed.y *= -1f;
		}
		if (other.tag == "Blocks") 
		{
			speed.y *= -1f;
			other.gameObject.GetComponent<BlockController>().Touch ();
		}
		if (other.tag == "Border2") 
		{
			speed.x *= -1f;
		}
		if (other.tag == "Player") 
		{
			speed.y *= -1f;
		} 
		else if (other.tag == "Finish") 
		{
			Reset ();
		}
	}

	void Reset()
	{
		transform.position = iniPos;
		speed = iniSpeed;
	}
}
