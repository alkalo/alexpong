﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player2 : MonoBehaviour {

	private Transform playerTransform;

	private Vector3 newPos;

	public float speed;
	private float move;

	void Start()
	{
		// Equivalente a playerTransform = GetComponent<Transform> ();
		playerTransform = transform;

		newPos = playerTransform.position;
	}

	void Update () 
	{
		/*
		//Read input
		move = Input.GetAxis("Vertical");


		//Apply speed
		move *= speed;


		//Limit position
		newPos = new Vector3 (newPos.x, playerTransform.position.y + move, newPos.z );

		if (newPos.y < -4.4f) 
		{
			newPos = new Vector3 (newPos.x, -4.4f, newPos.z);
		}
		if (newPos.y > 4.4f) 
		{
			newPos = new Vector3 (newPos.x, 4.4f, newPos.z);
		}

		//Set new position
		playerTransform.position = newPos;
		*/

		move = Input.GetAxis("Horizontal") * speed;
		move *= Time.deltaTime;
		move *= speed;
		transform.Translate(move, 0, 0);

		if (transform.position.x < -7.80f) {
			transform.position = new Vector3 (-7.80f, transform.position.y, transform.position.z);
		}else if(transform.position.x > 7.79f) {
			transform.position = new Vector3 (7.79f, transform.position.y, transform.position.z);
		}
	}
}
