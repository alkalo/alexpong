﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockController : MonoBehaviour {

	public int vidas;
	[SerializeField] Material oneLive;
	[SerializeField] Material twoLive;
	[SerializeField] Material treeLive;

	void Awake (){
		ChangeColor ();
	}

	void ChangeColor(){

		Material[] mats = GetComponent<Renderer> ().materials;
		if (vidas == 1) {
			mats [0] = oneLive;
		}else if (vidas == 2){
			mats [0] = twoLive;
		}else if (vidas == 3){
			mats [0] = treeLive;
			
		}
		GetComponent<Renderer> ().materials = mats;
	}

	public void Touch (){
		vidas--;
		if (vidas <= 0) {
			gameObject.SetActive (false);

		} else {
			ChangeColor ();
		}
	}
}
