﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ball : MonoBehaviour {
	int contador;
	public Text puntuacion;
	public Vector2 speed;

	private Vector2 iniSpeed;
	private Vector3 iniPos;

	void Awake(){
		contador = 0;
		puntuacion.text = "Puntuación: "+contador;
	}
	// Use this for initialization
	void Start () 
	{
		iniSpeed = speed;
		iniPos = transform.position;
	}

	// Update is called once per frame
	void Update () 
	{
		transform.Translate (speed.x*Time.deltaTime, speed.y*Time.deltaTime, 0);
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "Bounds") 
		{
			speed.y *= -1.01f;
			contador++;
			puntuacion.text = "Puntuación: "+contador;
		}
		if (other.tag == "Bounds2") 
		{
			speed.x *= -1.101f;
			contador++;
			puntuacion.text = "Puntuación: "+contador;
		}
		if (other.tag == "Player") 
		{
			speed.x *= -1.01f;

		} 
		if (other.tag == "Enemigo") 
		{
			speed.y *= -1.01f;

		} 
		if (other.tag == "Enemigo2") 
		{
			speed.x *= -1.01f;

		} 
		else if (other.tag == "Goal") 
		{
			Reset ();
			contador = 0;
		}
	}

	void Reset()
	{
		transform.position = iniPos;
		speed = iniSpeed;
	}
}
