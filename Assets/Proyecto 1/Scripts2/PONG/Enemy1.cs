﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy1 : MonoBehaviour {

	private Transform playerTransform;
	private Vector3 newPos;

	public Transform ballPosition;

	public float speed;
	private float move;

	void Start()
	{
		// Equivalente a playerTransform = GetComponent<Transform> ();
		playerTransform = transform;

		newPos = playerTransform.position;
	}

	void Update () 
	{
		if (ballPosition.position.x > transform.position.x) {
			move = 1;
		} else {
			move = -1;
		}
		move *= Time.deltaTime;
		move *= speed;
		transform.Translate(move, 0, 0);

		if (transform.position.x < -12.93f) {
			transform.position = new Vector3 (-12.93f, transform.position.y, transform.position.z);
		}else if(transform.position.x > 5.15f) {
			transform.position = new Vector3 (5.15f,transform.position.y, transform.position.z);
		}
	}
}
