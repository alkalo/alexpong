﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;


public class UIController : MonoBehaviour {

	public Animator infoWindow;

	public GameObject configWindow;


	public Animator transation;

	//Time bar control from info window
	public TimeBar infoActivation;


	//-- INFORMATION WINDOW --

	public void OpenInfoWindow () {
		infoActivation.Activate();
		infoWindow.SetBool("Open",true);
	}

	public void CloseInfoWindow () {
		infoWindow.SetBool("Open",false);
	}


	//-- CONFIG WINDOW --

	public void OpenConfigWindow(){
		configWindow.SetActive (true);
	}


	public void CreditosT(){
		transation.SetTrigger("Creditos");
	}
	
	public void FullScreen(){
		Screen.fullScreen = !Screen.fullScreen;
	}


}
