﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class OpenExitGame : MonoBehaviour {

	bool opengame = false;
	float time = 4f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(opengame){
			time -= Time.deltaTime;
			if (time <= 0f){

				SceneManager.LoadScene("Pong3D");
			}

		}
	}

	public void OpenGame(){
		opengame = true;

	}


	public void ExitGame(){
		Application.Quit();
	}
}
