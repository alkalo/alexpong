﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TimeBar : MonoBehaviour {

	//Tiempo que tardara la barra en cargarse y la ventana en cerrarse
	public int timeSecs;

	//Referencia a la imagen de barra de carga
	public Image timeBar;

	//Referencia al control de ventanas de este UI
	public UIController widowControl;

	//Tiempo transcurrido
	private float elapsedTime = 0;

	//Indicador de que estamos cerrando la ventana
	private bool active = false;

	//Activamos la ventana y reinciamos el contador de tiempo
	public void Activate () {
		active = true;
		elapsedTime = 0;
	}

	// Update is called once per frame
	void Update () {

		if (active) {
			//Incrementamos el tiempo transcurrido
			elapsedTime += Time.deltaTime;

			//Si se completa el tiempo...
			if (elapsedTime > timeSecs) {
				//cerramos la ventana.
				widowControl.CloseInfoWindow();
				//Desactivamos la ventana
				active = false;
			}
			else
				//Si no, aumentamos la barra de tiempo.
				timeBar.fillAmount = elapsedTime / timeSecs;
		}
	}
}
