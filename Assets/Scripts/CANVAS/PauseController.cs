﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PauseController : MonoBehaviour {
	public GameObject can;
	public GameObject MusicaPanel;
	public GameObject OptionPanel;
	public GameObject AyudaPanel;





	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown(KeyCode.Escape)){
			if(Time.timeScale == 0f){
				Time.timeScale = 1f;
				can.SetActive(false);
			}else if (Time.timeScale == 1f){
				can.SetActive(true);
				Time.timeScale = 0f;
			}
		}
			




	}

	public void Canactive(){
		if(Time.timeScale == 0f){
			Time.timeScale = 1f;
			can.SetActive(false);
		}else if (Time.timeScale == 1f){
			can.SetActive(true);
			Time.timeScale = 0f;
		}
	}

	public void Cancelar(){
		Time.timeScale = 1f;
		can.SetActive(false);
	}

	public void SalirDelJuego(){
		Application.Quit();
	}

	public void Musica(){
		MusicaPanel.SetActive(true);
	}

	public void MusicaOFF(){
		MusicaPanel.SetActive(false);
	}

	public void Opciones(){
		OptionPanel.SetActive(true);
	}

	public void OpcionesOFF(){
		OptionPanel.SetActive(false);
	}


	public void Ayuda(){
		AyudaPanel.SetActive(true);
	}

	public void AyudaOFF(){
		AyudaPanel.SetActive(false);
	}

}
